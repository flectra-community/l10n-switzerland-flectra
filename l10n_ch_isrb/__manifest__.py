# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Switzerland - ISR-B",
    "summary": "Switzerland - ISR with Bank",
    "version": "2.0.1.0.0",
    "author": "Camptocamp,Odoo Community Association (OCA), 2BIT GmbH",
    "category": "Localization",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "AGPL-3",
    "depends": ["l10n_ch"],
    "data": ["views/res_partner_bank.xml"],
}
