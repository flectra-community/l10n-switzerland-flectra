# copyright 2016 Akretion - Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from flectra import models, fields


class AccountPaymentLine(models.Model):
    _inherit = 'account.payment.line'

    local_instrument = fields.Selection(
        selection_add=[('CH01', 'CH01 (ISR)')])
    communication_type = fields.Selection(selection_add=[
        ('isr', 'ISR'),
        ('QRR', 'QR-Bill with QR-IBAN and Reference Number'),
        ('SCOR', 'QR-Bill with IBAN and Reference Text'),
    ],ondelete={'isr': 'cascade','QRR':'cascade','SCOR':'cascade'})

    def invoice_reference_type2communication_type(self):
        res = super(AccountPaymentLine, self).\
            invoice_reference_type2communication_type()
        res['isr'] = 'isr'
        res['QRR'] = 'QRR'
        res['SCOR'] = 'SCOR'
        return res
