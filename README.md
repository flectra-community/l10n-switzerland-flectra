# Flectra Community / l10n-switzerland-flectra

# l10n-switzerland

This is the maitained repo of l10n_ch extension by the flectra community. If you are using this repo you are not allowed to use l10n_ch modules from the auto converted repository.

<!-- /!\ do not modify below this line -->

<!-- prettier-ignore-start -->

[//]: # (addons)

Available addons
----------------
| addon                                                              | version   | summary                                                         |
|--------------------------------------------------------------------|-----------|-----------------------------------------------------------------|
| [ebill_paynet](ebill_paynet)                                       | 2.0.1.0.0 | Exchange with SIXT Paynet infrastructure                        |
| [ebill_paynet_customer_free_ref](ebill_paynet_customer_free_ref)   | 2.0.1.0.0 | eBill Paynet Customer Free Reference                            |
| [l10n_ch_account_payment_partner](l10n_ch_account_payment_partner) | 2.0.1.0.0 | Set bank on customer invoices                                   |
| [l10n_ch_account_tags](l10n_ch_account_tags)                       | 2.0.1.0.0 | Switzerland Account Tags                                        |
| [l10n_ch_base_bank](l10n_ch_base_bank)                             | 2.0.1.0.0 | Types and number validation for swiss electronic pmnt. DTA, ESR |
| [l10n_ch_hr_payroll](l10n_ch_hr_payroll)                           | 2.0.1.0.0 | Switzerland Payroll Rules                                       |
| [l10n_ch_hr_payroll_report](l10n_ch_hr_payroll_report)             | 2.0.1.0.0 | Switzerland Payroll Report                                      |
| [l10n_ch_invoice_reports](l10n_ch_invoice_reports)                 | 2.0.1.0.0 | Extend invoice to add ISR/QR payment slip                       |
| [l10n_ch_isr_payment_grouping](l10n_ch_isr_payment_grouping)       | 2.0.1.0.0 | Switzerland - ISR payment grouping                              |
| [l10n_ch_isrb](l10n_ch_isrb)                                       | 2.0.1.0.0 | Switzerland - ISR-B                                             |
| [l10n_ch_pain_base](l10n_ch_pain_base)                             | 2.0.1.0.0 | Switzerland - ISO 20022 base module                             |
| [l10n_ch_pain_credit_transfer](l10n_ch_pain_credit_transfer)       | 2.0.1.0.0 | Switzerland - ISO 20022 credit transfer                         |
| [l10n_ch_report_optional](l10n_ch_report_optional)                 | 2.0.1.0.0 | Settings to add QR or ISR optional to report                    |
| [l10n_ch_states](l10n_ch_states)                                   | 2.0.1.0.0 | Switzerland Country States                                      |
| [l10n_ch_zip](l10n_ch_zip)                                         | 2.0.1.0.0 | Swiss postal code (ZIP) list                                    |
| [server_env_ebill_paynet](server_env_ebill_paynet)                 | 2.0.1.0.0 | Server environment for Ebill Paynet                             |

[//]: # (end addons)

<!-- prettier-ignore-end -->

## Licenses

This repository is licensed under [AGPL-3.0](LICENSE).

However, each module can have a totally different license, as long as they adhere to FC
policy. Consult each module's `__manifest__.py` file, which contains a `license` key file that explains its license.

----

FC, or the [Flectra Community](http://flectra-community.org/), is a nonprofit
organization whose mission is to support the collaborative development of Flectra features
and promote its widespread use.
