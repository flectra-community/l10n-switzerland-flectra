# Copyright 2014-16 Akretion - Alexis de Lattre <alexis.delattre@akretion.com>
# Copyright 2014 Serv. Tecnol. Avanzados - Pedro M. Baeza
# Copyright 2021 Tecnativa - Víctor Martínez
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

from flectra import api, fields, models


class AccountMove(models.Model):
    _inherit = "account.move"

    @api.depends("partner_id", "payment_mode_id")
    def _compute_partner_bank(self):
        for move in self:
            # No bank account assignation is done for out_invoice as this is only
            # needed for printing purposes and it can conflict with
            # SEPA direct debit payments. Current report prints it.
            def get_bank_id():
                return move.commercial_partner_id.bank_ids.filtered(
                    lambda b: b.company_id == move.company_id or not b.company_id
                )[:1]

            bank_id = False
            if move.partner_id:
                pay_mode = move.payment_mode_id
                if move.move_type == "in_invoice":
                    if (
                        pay_mode
                        and pay_mode.payment_type == "outbound"
                        and pay_mode.payment_method_id.bank_account_required
                        and move.commercial_partner_id.bank_ids
                    ):
                        bank_id = get_bank_id()
                elif move.move_type == 'out_invoice':
                    if pay_mode.bank_account_link == 'fixed':
                        bank_id = pay_mode.fixed_journal_id.bank_account_id

            if bank_id:
                move.partner_bank_id = bank_id
