# -*- coding: utf-8 -*-
# Part of Odoo, Flectra. See LICENSE file for full copyright and licensing details.
# Main contributor: Nicolas Bessi. Camptocamp SA
# Financial contributors: Hasa SA, Open Net SA,
#                         Prisme Solutions Informatique SA, Quod SA
# Translation contributors: brain-tec AG, Agile Business Group
{
    'name': "Switzerland - Accounting Optional Reports as Attachments",
    'description': """
        Add some parameters to define if ISR or QR Bill should be added automatically
    """,
    'version': '1.0.1.0',
    'category': 'Accounting',
    'depends': ['l10n_ch'],
    'data': [
        'views/res_config_settings_views.xml',
    ],
    'license': 'LGPL-3',
}
