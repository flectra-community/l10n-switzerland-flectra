# Copyright 2017 Open Net Srl
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from flectra import fields, models, api


class HrContract(models.Model):
    _inherit = 'hr.contract'

    lpp_rate = fields.Float(
        string='OBP Rate (%)',
        digits='Payrollrate')
    lpp_amount = fields.Float(
        string='OBP Amount',
        digits='Account')
    lpp_contract_id = fields.Many2one(
        string='OBP Contract',
        comodel_name='lpp.contract',
        #inverse_name='contract_id'
        )

    imp_src = fields.Float(
        string='Source Tax (%)',
        digits='Payrollrate')

    wage_type = fields.Selection(
        string="Wage Type",
        selection=[('month', "Monthly"), ('hour', "Hourly")],
        default='month')
    wage_fulltime = fields.Float(
        string='Full-time Wage',
        digits='Account',
        default=0)
    occupation_rate = fields.Float(
        string='Occupation Rate (%)',
        digits='Account',
        default=100.0)

    @api.onchange('occupation_rate', 'wage_fulltime')
    def _onchange_wage_rate_fulltime(self):
        for contract in self:
            contract.wage = \
                contract.wage_fulltime * (contract.occupation_rate / 100)
