# Copyright 2020 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html)

{
    "name": "Server environment for Ebill Paynet",
    "version": "2.0.1.0.0",
    "author": "Camptocamp,Odoo Community Association (OCA), 2BIT GmbH",
    "license": "AGPL-3",
    "category": "Tools",
    "depends": ["server_environment", "ebill_paynet"],
    "website": "https://gitlab.com/flectra-community/flectra",
    "installable": True,
}
