# Copyright 2021 Camptocamp SA
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "eBill Paynet Customer Free Reference",
    "summary": "Glue module: ebill_paynet and sale_order_customer_free_ref",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "Camptocamp SA,Odoo Community Association (OCA), 2BIT GmbH",
    "website": "https://gitlab.com/flectra-community/flectra",
    "depends": ["ebill_paynet", "sale_order_customer_free_ref"],
    "auto_install": True,
}
