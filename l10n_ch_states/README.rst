==========================
Switzerland Country States
==========================

.. !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !! This file is generated by oca-gen-addon-readme !!
   !! changes will be overwritten.                   !!
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 

This module extends res.country.states for Switzerland. It brings states with
code and name for those who only want the states without cities.

For those who also want cities, please install *Switzerland - Postal codes (ZIP) list*  (`l10n_ch_zip`).

**Table of contents**

.. contents::
   :local:

Usage
=====

In order to use this module you just in to install it. It will add Switzerland
country states to your database

Credits
=======

Authors
~~~~~~~

* copado MEDIA UG

Contributors
        ------------

        * Jamotion <info@jamotion.ch>
* Mathias Neef <mn@copado.de>