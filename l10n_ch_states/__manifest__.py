# Copyright 2019-2020 Camptocamp SA
# Copyright 2015 Mathias Neef copadoMEDIA UG
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).
{
    "name": "Switzerland Country States",
    "category": "Localisation",
    "summary": "",
    "version": "2.0.1.0.0",
    "author": "copado MEDIA UG,, 2BIT GmbH" "Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/flectra",
    "license": "AGPL-3",
    "depends": ["base"],
    "data": ["data/res_country_states.xml"],
}
