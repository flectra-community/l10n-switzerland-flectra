.. image:: https://img.shields.io/badge/licenceAGPL3blue.svg
   :target: http://www.gnu.org/licenses/agpl3.0standalone.html
   :alt: License: AGPL3


Switzerland  Payroll Reports


This module extends the functionality of payroll to:

* Customize the Pay Slip report
* Add a Pay Slip Yearly report

These 2 reports follow the Swissdec guidelines (p.84 and p.87) (Sorry PDF is in french):
https://www.swissdec.ch/fileadmin/user_upload/SoftwareHersteller/RL4.0/RichtlinienLohndatenverarbeitung20130514_20130519_f.pdf

Configuration


To configure this module, you need to:

#. Go to each salary rule and define the "Report options"

Usage


To use this module, you need to:

* Print a pay slip report
* Or go to menu "Payroll > Reports > Pay Slip Yearly Report" to access to the Pay Slip Yearly report

Credits


Images


* Odoo Community Association: `Icon <https://github.com/OCA/maintainertools/blob/master/template/module/static/description/icon.svg>`_.

Contributors
        

* Jamotion <info@jamotion.ch>
* Julien Coux <julien.coux@camptocamp.com>

Funders


The development of this module has been financially supported by:

* Camptocamp